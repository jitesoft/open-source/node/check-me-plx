# check-me-plx

An application with plugin and alert rules to check availability of services and endpoints.

Observe, this project is only in early stage and not yet ready for production. Await the 1.0.0 version if you wish to use
it for production or jump in and help with the development to speed things up!

## Development

To start the development environment, just install the required npm packages and run the `dev` script.

```bash
npm i
npm run dev
```

This will start the API server (using nodemon to restart on changes) on `localhost:3000` and the frontend on `localhost:3001` using 
the webpack-dev-server package to hot-swap any changed modules of the code.

Both the api and the frontend uses webpack and babel to convert latest style javascript (with some extra sugar) to javascript suitable for
both node and client.  
The intended node version is latest, which as of writing this is 11.6.x.

Check the package.json file for more information about packages used.

### Backend

The backend uses Hapi for serving the API. It is intended to be a rest-api when it comes to contact with the client/s, while the 
workers will be plugable services that runs async on set times.

### Frontend

Currently the frontend have no framework or spec, this will be updated when it is decided.

## Licenses

This project will be released under the MIT license as long as it is applicable to the licenses implemented by the packages used.  
This part of the readme will be updated with license information before the 1.0.0 release.
