import 'dotenv/config';
import './log';
import Hapi from 'hapi';
import Logger from 'winston';
import Workers from './worker';
import { Router } from './actions';

const server = Hapi.Server({
  port: 3000,
  host: '127.0.0.1'
});

/**
 * @param {...Object} plugins
 * @returns {Promise<void>}
 */
const setUp = async (...plugins) => {
  for (let plg in plugins) {
    await server.register(plg);
  }

  // Actions.
  const router = new Router(server);
  await router.init();

  // Workers.

  await Workers.init();
  await server.start();
  Logger.info(`API-Server started, listening on ${server.info.uri}`);
  Logger.debug(`Environment: ${process.env.NODE_ENV}.`);
  if (process.env.NODE_ENV === 'development') {
    Logger.debug('Development frontend served on http://localhost:3001');
  }
};

setUp();
