import winston from 'winston';

// const env = process.env.NODE_ENV || 'development';
// Load configuration and set what type of logging to use, but for now, dump all to stdout.
const format = winston.format.printf((msg) => {
  return `[${msg.level}] (${msg.timestamp}): ${msg.message}`;
});

winston.remove(winston.transports.Console);
winston.configure({
  level: 'debug',
  format: winston.format.combine(
    winston.format.timestamp(),
    format
  ),
  transports: [
    new winston.transports.Console()
  ]
});
