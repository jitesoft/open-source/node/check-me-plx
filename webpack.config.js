const Path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
let env = 'production';
if (process.env.NODE_ENV === 'development') {
  env = 'development';
}

const client = {
  name: 'client',
  mode: env,
  target: 'web',
  entry: {
    'client': Path.join(__dirname, 'src', 'client', 'index.js')
  },
  output: {
    path: Path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'check-me-plx',
    globalObject: "typeof self !== 'undefined' ? self : this"
  },
  devServer: {
    port: 3001,
    clientLogLevel: 'info',
    disableHostCheck: true,
    compress: true,
    hot: true,
    index: 'index.html',
    overlay: {
      warnings: true,
      errors: true
    },
    proxy: {
      '/api/v1': 'http://localhost:3000'
    },
    stats: 'minimal',
    watchContentBase: true
  },
  devtool: 'source-map',
  optimization: {
    minimize: env === 'production'
  },
  plugins: [
    new HtmlPlugin({
      title: 'Check me plx!',
      favicon: Path.resolve(__dirname, 'src', 'favicon.svg'),
      hash: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        include: [Path.join(__dirname, 'src', 'client')],
        loader: 'babel-loader'
      }
    ]
  }
};

const server = {
  name: 'server',
  mode: env,
  target: 'node',
  entry: {
    'server': [
      Path.join(__dirname, 'src', 'server', 'index.js')
    ]
  },
  output: {
    path: Path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    globalObject: "typeof self !== 'undefined' ? self : this",
    libraryTarget: 'commonjs'
  },
  optimization: {
    minimize: false
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        include: [Path.join(__dirname, 'src', 'server')],
        loader: 'babel-loader'
      }
    ]
  }
};

module.exports = [
  client, server
];
